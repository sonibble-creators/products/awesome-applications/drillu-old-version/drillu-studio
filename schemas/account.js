import { User } from "react-iconly";

/**
 * # Account
 *
 * account schema is the user type definition data
 * contain all data about the user and the basic until the advance one
 */
export default {
  name: "account",
  title: "Account",
  type: "document",
  icon: User,

  // specify the field will be using
  fields: [
    {
      name: "avatar",
      title: "Avatar",
      type: "image",
      options: { hotspot: true },
    },
    {
      name: "cover",
      title: "Cover",
      type: "image",
      options: { hotspot: true },
    },
    {
      name: "email",
      title: "Email",
      description: "The email address of the account",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "username",
      title: "Username",
      description: "The username of the account",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "password",
      title: "Password",
      description: "The password of the account",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "fullName",
      title: "Full Name",
      description: "The full name of the account",
      type: "string",
    },
    {
      name: "location",
      title: "Location",
      description: "Location name where the account live",
      type: "string",
    },
    {
      name: "skills",
      title: "Skills",
      description: "the creative skill account have",
      type: "array",
      of: [{ type: "string" }],
    },
    {
      name: "status",
      title: "Status",
      description: "Status of the account",
      type: "string",
      options: {
        list: [
          { title: "Active", value: "active" },
          { title: "Disable", value: "disable" },
        ],
      },
    },
    {
      name: "roles",
      title: "Roles",
      description: "Roles of the account",
      type: "array",
      of: [{ type: "string" }],
      options: {
        list: [
          { title: "User", value: "user" },
          { title: "Admin", value: "admin" },
          { title: "Creator", value: "creator" },
        ],
      },
    },
    {
      name: "bio",
      title: "Bio",
      description: "The bio of the account",
      type: "text",
    },
  ],

  // set the initial value
  // some of the field may need a default value
  initialValue: {
    status: "active",
  },

  // orderings
  // when the user want to order the listed data in document
  // the order is set here
  orderings: [],

  // preview allow to set some data
  // that need to show when in the listed
  preview: {
    select: {
      title: "fullName",
      subtitle: "username",
      media: "avatar",
    },
  },
};
