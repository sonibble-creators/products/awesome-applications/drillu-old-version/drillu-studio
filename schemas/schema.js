import createSchema from "part:@sanity/base/schema-creator";
import schemaTypes from "all:part:@sanity/base/schema-type";
import Account from "./account";

// create the schema with the types you want to use
// come with the default types (object, array, string, number, boolean)
export default createSchema({
  name: "drilluschema",
  types: schemaTypes.concat([
    // TODO: Import all of the schema we created
    Account,
  ]),
});
